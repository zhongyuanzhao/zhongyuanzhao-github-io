---
title: "Deep-Waveform: A Learned OFDM Receiver Based on Deep Complex Convolutional Networks"
category: 'preprint'
collection: publications
permalink: /publications/2018-10-23-Deep-Waveform.html
excerpt: 'This paper is about using complex valued deep neuron networks to implement the lower physical layer of OFDM-based wireless communications. It demonstrates that complex neuron networks could learn the complex waveform used in modern wireless network, and outperforms analytical channel estimation approach in Rayleigh fading under certain conditions.'
date: 2018-10-23
venue: 'Arxiv'
paperurl: 'https://arxiv.org/abs/1810.07181'
citation: 'Zhongyuan Zhao, Mehmet C. Vuran, Fujuan Guo, and Stephen Scott, &quot;Deep-Waveform: A Learned OFDM Receiver Based on Deep Complex Convolutional Networks,&quot; <i>Arxiv</i>, EESS.SP, Oct 2018.'
---
Abstract
===
Recent explorations of Deep Learning in the physical layer (PHY) of wireless communication have shown the capabilities of Deep Neuron Networks in tasks like channel coding, modulation, and parametric estimation. However, it is
unclear if Deep Neuron Networks could also learn the advanced waveforms of current and next-generation wireless networks, and potentially create new ones. In this paper, a Deep Complex Convolutional Network (DCCN) without explicit Discrete Fourier Transform (DFT) is developed as an Orthogonal FrequencyDivision Multiplexing (OFDM) receiver. Compared to existing
deep neuron network receivers composed of fully-connected layers followed by non-linear activations, the developed DCCN not only contains convolutional layers but is also almost (and could be fully) linear. Moreover, the developed DCCN not only learns to convert OFDM waveform with Quadrature Amplitude
Modulation (QAM) into bits under noisy and Rayleigh channels, but also outperforms expert OFDM receiver based on Linear Minimum Mean Square Error channel estimator with prior channel knowledge in the low to middle Signal-to-Noise Ratios of Rayleigh channels. It shows that linear Deep Neuron Networks could learn transformations in signal processing, thus master advanced waveforms and wireless channels.

_Keywords_: Wireless Communications, Physical Layer, Deep Learning, Artificial Neuron Networks, OFDM, Modulation

[Download paper here](https://arxiv.org/pdf/1810.07181.pdf)

Recommended citation: 

Zhongyuan Zhao, Mehmet C. Vuran, Fujuan Guo, and Stephen Scott, "Deep-Waveform: A Learned OFDM Receiver Based on Deep Complex Convolutional Networks," Arxiv, EESS.SP, Oct. 2018, [Online] [https://arxiv.org/abs/1810.07181](https://arxiv.org/abs/1810.07181)