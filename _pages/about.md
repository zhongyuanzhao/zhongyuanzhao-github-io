---
permalink: /
title: "About"
excerpt: "Biography"
author_profile: true
redirect_from:
  - /about/
  - /about.html
---
{% include base_path %}

I am a Postdoctoral Research Associate in the Department of Electrical and Computer Engineering at Rice University. I work with [Prof. Santiago Segarra](http://segarra.rice.edu/) on topics of using deep learning, especially graph neural networks and reinforcement learning, to improve the wireless networks.

I got my Ph.D. degree from the Department of Computer Science and Engineering of University of Nebraska-Lincoln in 2019, where I worked with [Dr. Mehmet C. Vuran](http://cse.unl.edu/~mcvuran/) on [dynamic spectrum sharing]({{site.baseurl}}/portfolio/cognitive-radio-networks/). Before my Ph.D program, I earned my B.Sc. and M.Sc. in Electronic Engineering from University of Electronic Science and Technology of China, and worked in ArrayComm and Ericsson developing 4th generation wireless base-stations.


Recent News
======
  <ul>{% for post in site.posts limit:5 reversed %}
    {% include archive-single-talk-cv.html %}
  {% endfor %}</ul>

A little bit more about me
======

I do some extracurricular activities:
* UNL Deep Learning Slack Team
* International Student Fellowship
* Toastmasters
* Swimming
* Basketball
