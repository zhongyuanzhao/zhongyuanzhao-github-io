---
title: "Business Training"
excerpt: "CFA Exams, Business Courses, and Investment Practice<br/><img src='https://cse.unl.edu/~zhzhao/images/books-1.jpg'>"
collection: portfolio
---
CFA Exams, Accomplished Business Courses, and Trading Track Record.

## CFA
* CFA Level I exam passed in Dec. 2017,
* CFA Level II exam passed in Jun. 2018,

## Accomplished Courses 

* UNL CBA [GRBA 811: Managerial Finance](https://bulletin.unl.edu/courses/GRBA/811) (Fall 2015),
* UNL CBA [MNGT 864: Talent Acquisition and Staffing](https://bulletin.unl.edu/courses/MNGT/864) (Spring 2018),
* UNL CBA [FINA 862: Security Valuation and the Buffett Investment Method](https://bulletin.unl.edu/courses/FINA/862) (Spring 2017),
* UNL CBA [FINA 855: Capital Markets and Financial Institutions](https://bulletin.unl.edu/courses/FINA/855) (Spring 2017),
* __Operational Finance: Finance for Managers__ by IESE Business School on Coursera. Grade Achieved: 96.2%; [Certificate](https://www.coursera.org/account/accomplishments/records/RPR64324CLQX) earned at Tuesday, May 23, 2017 12:12 AM GMT,
* __Organizational Behavior: How to Manage People__ by IESE Business School on Coursera. Grade Achieved: 87.8%; [Certificate](https://www.coursera.org/account/accomplishments/records/AX5VG8P9BF5U) earned at Friday, January 6, 2017 1:58 PM GMT, 
* __Accounting: Principles of Financial Accounting__ by IESE Business School on Coursera. Grade Achieved: 94.6%; [Certificate](https://www.coursera.org/account/accomplishments/records/Q4UJE35PQEZ3) earned at Thursday, January 19, 2017 6:50 AM GMT.

## Audited Track Record <a name="trackrecord"></a>

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSXqd0OC7stiquegoWzvteIgUdHwJrA9N3_Tw0dExeWOzH61jOdXnOwgBjIt8sP_Bhh4I98geRMqbuZ/pubchart?oid=1051024560&amp;format=interactive"></iframe>

Full track record and verification from 3rd party auditor available upon request.

## Talks
* [Healthcare Reform: It Is All About Cost & Accountability]({{site.baseurl}}/talks/2018-11-15-life-insurance.html)
* [High Frequency Trading: Explanation, Status, And Implication]({{site.baseurl}}/talks/2017-05-07-capital-markets-final.html)

## Why business?

>Why NOT? Most of us have a role in this industrialized society. Not teaching people how businesses work is just as wrong as not offering universal education on basics of democracy and computer skills. That bug should be fixed. I think the founders of [Jeffrey S. Raikes School of Computer Science and Management ](https://raikes.unl.edu/) would probably agree with me :) 

